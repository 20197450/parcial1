﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial_cajero
{
    class Program
    {
        static void Main(string[] args)
        {
            //PRIMER PARCIAL DE F... PROGRAMACIÓN. 2019-7450
            /* 
            Cree una aplicación de cajero automático para el banco FDP INVERSMENTS. 
            El cajero tendrá un límite de billetes descrito a continuación: 
                        18 billetes de 1,000
                        19 billetes de 500
                        23 billetes de 200
                        50 billetes de 100
            El cajero debe solicitar Banco y monto a retirar. Si el banco es FDP INVERSMENTS 
            el limite de retiro es 20,000 y 10,000 pesos por transacción en caso contrario.

            El cajero debe informar si el monto solicitado no puede ser dispensado o si excede el 
            límite de transacción. Y debe hacer la distribución de los billetes de acuerdo al monto.  
            */

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // "Cantidad Maxima Del Monto a Retirar" "Cantidad Maxima Del Monto de Transación"
            int CMDMR = 20000, CMDMT = 10000;

            Console.WriteLine("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
            Console.WriteLine("*******************************Le damos la bienvenida a nuestro Cajero FDP Inversments!!*******************************\n");
            Console.WriteLine("                                     Por favor, seleccione su banco deseado.\n");
            Console.WriteLine("           1) BHD Leon           2) FDP Inversments           3) Banco Ademi           4) Terminar\n");
            Console.WriteLine("________________________________________________________________________________________________________________________");

            Console.Write("DIGITE NUMERO DE SU BANCO: ");
            Console.Write("");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                //*NOTA*  HAGO ESTAS LINEAS PARA EVITAR CONFUNDIRME
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //"B" de Banco 
            int B = Convert.ToInt32(Console.ReadLine());
            if (B == 1 || B == 2 || B == 3)

            {
                Console.WriteLine("¿Qué desea realizar? \n" +
                    "1. Retiro \n" +
                    "2. Transacción \n" +
                    "3. Terminar");
                Console.Write("DIGITE: ");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //"O" de opción
                int O = Convert.ToInt32(Console.ReadLine());
                if (O == 1)
                {
                    Console.WriteLine($"Favor de ingresar la cantidad que desea retirar; El limite a retirar es de ${CMDMR}");
                    Console.Write("DIGITE: ");
                    int Montoaretirar = Convert.ToInt32(Console.ReadLine());
                    if (Montoaretirar <= CMDMR)
                    {
                        Console.Write($"La cantidad a Retirar es de {Montoaretirar} \n");
                        Console.WriteLine("Seleccione el metodo de distribucción deseado. \n" +
                            "1. Billetes de 1,000 \n" +
                            "2. Billetes de 500 \n" +
                            "3. Billetes de 200 \n" +
                            "4. Billetes de 100");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        int distribución = Convert.ToInt32(Console.ReadLine());
                        if (distribución == 1)
                        {
                            int Retiro = Montoaretirar / 1000;
                            Console.Write($"El monto es un total de {Retiro} Billetes de 1,000 \n");
                            Console.Write("Gracias por preferirnos!");
                            Console.ReadKey();
                        }
                        else if (distribución == 2)
                        {
                            int Retiro2 = Montoaretirar / 500;
                            Console.Write($"El monto es un total de {Retiro2} Billetes de 500 \n");
                            Console.Write("Gracias por preferirnos!");
                            Console.ReadKey();
                        }
                        else if (distribución == 3)
                        {
                            int Retiro3 = Montoaretirar / 200;
                            Console.Write($"El monto es un total de {Retiro3} Billetes de 200 \n");
                            Console.Write("Gracias por preferirnos!");
                            Console.ReadKey();
                        }
                        else if (distribución == 4)
                        {
                            int Retiro4 = Montoaretirar / 100;
                            Console.Write($"El monto es un total de {Retiro4} Billetes de 100 \n");
                            Console.Write("Gracias por preferirnos");
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.Write("Favor de seleccionar una opción.");
                            Console.ReadKey();
                        }
                    }
                    else if (Montoaretirar > CMDMR)
                    {
                        Console.Write("%%%%%%%%%%%%%%%%%%% El monto ha superado el limite de Retiro %%%%%%%%%%%%%%%%%%%%");
                        Console.ReadKey();
                    }
                }
                else if (O == 2)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                {
                    Console.WriteLine($"Favor de ingresar la cantidad que desea retirar; El limite a transferir es de ${CMDMT}");
                    Console.Write("DIGITE: ");

                    int CantidadTrans = Convert.ToInt32(Console.ReadLine());
                    if (CantidadTrans <= CMDMT)
                    {
                        Console.WriteLine($"La cantidad a Transferir es de {CantidadTrans}");
                        int CuentaTrans;
                        do
                        {
                            Console.Write("Ingrese el número de cuenta a transferir: ");
                            CuentaTrans = Convert.ToInt32(Console.ReadLine());

                            Console.Write("_______________________________________________________________________________________________________\n");

                            Console.WriteLine($"Verificando el número de cuenta: {CuentaTrans}...");
                            if (CuentaTrans > 999999999)
                            {
                                Console.Write("El número de cuenta introducido es invalido");
                                Console.ReadKey();
                            }
                        }
                        while (CuentaTrans > 999999999);

                        if (CuentaTrans <= 999999999)
                        {
                            Console.WriteLine("El numero de cuenta ha sido validado");
                            Console.WriteLine("Favor de confirmar su transaccion... \n" +
                                "Confirmar \n" +
                                "No");
                            string respuesta = Console.ReadLine();
                            if (respuesta == "Confirmar" || respuesta == "confirmar")
                            {
                                Console.Write("***********LISTO!! SE HA REALIZADO LA TRANSACCIÓN.***********");
                                Console.ReadKey();
                            }
                            else if (respuesta == "No" || respuesta == "no \n")
                            {
                                Console.WriteLine("Gracias por preferirnos!");
                                Console.ReadKey();
                            }
                        }
                    }
                }
                else if (O == 3)
                {
                    Console.WriteLine("***************************************¡GRACIAS POR PREFERIRNOS!***************************************");


                    Console.ReadKey();
                }
            }
        }
    }
}
